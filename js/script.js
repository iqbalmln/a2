// console
// console.log('Hello World');
// console.error('This is an error');
// console.warn('This is a warning');

// Variable
// var, let, const

// Data types
// String, number, boolean, null, undifined, symbol
// const name = 'Iqbal';
// const age = 30;
// const rating = 4.5;
// const isCool = true;
// const x = null
// const y = undefined
// let z;

// console.log(typeof z);

// Concat
// console.log('my name is ' + name + ' and i am '+ age +' years old');
// templated literals
// console.log(`my name is ${name}, and i am ${age} years old`);

// String method
// const str = 'tech, marketing, visual'
// console.log(str.toUpperCase());
// console.log(str.toLowerCase());
// console.log(str.substring(0,5).toUpperCase());
// console.log(str.split(''));
// console.log(str.split(',')); //string into array

// Arrays - Variables that hold multiples value
// const fruits = ["Mangos", "Apples", "Bananas", "Watermelon"];
// fruits.push("Jambu") // add item into array at the end item of array
// fruits.unshift("Rambutan") // add item into array at the first item of array
// console.log(fruits);


// Object
// Object Literal, Contructor
// const person = {
//   firstName: 'Iqbal',
//   lastName: 'Maulana',
//   age: 22,
//   hobbies: ['music', 'football', 'movies'],
//   address: {
//     street: '22 Edelweiss',
//     city: 'Malang',
//     state: 'Jawa Timur'
//   }
// }
// console.log(person);

// // Destructuring Object
// const { firstName, lastName } = person;
// console.log(firstName);

// Array of Object
const todos = [
  {
    id: 1,
    text: 'Take out trash',
    isCompleted: true
  },
  {
    id: 2,
    text: 'Meeting w/ boss',
    isCompleted: true
  },
  {
    id: 1,
    text: 'Dentist appt',
    isCompleted: false
  }
]
// const todoJSON = JSON.stringify(todos)
// console.log(todoJSON);

// Looping
// For, while
// for(let i = 0; i < todos.length; i++) {
//   console.log(todos[i].text);
// }

// let  i = 0
// while(i < 10) {
//   console.log(i);
//   i++
// }

// for of..
// for(let todo of todos) {
//   console.log(todo.text);
// }

// hight order function
// forEach, map, filter, reduce, etc

// forEach
// todos.forEach(todo => console.log(todo))

// map
// const todoText =  todos.map(todo => console.log(todo.text))

// filter
// const todosCompleted = todos.filter(todo => todo.isCompleted ? todo : '').map(todo => todo.text)
// console.log(todosCompleted);

// Condition
// const x = 10
// const y = 5
// if(x === 10) {
//   console.log('x is 10');
// } else {
//   console.log('x is not 10');
// }

// Constructor Function
// function Person(firstName, lastName, dob) {
//   this.firstName = firstName
//   this.lastName = lastName
//   this.dob = dob
// }
// const iqbal = new Person('iqbal', 'maulanna', '2000-15-11')
// console.log(iqbal);

// class
// class Person {
//   constructor(firstName, lastName, dob) {
//     this.firstName = firstName
//     this.lastName = lastName
//     this.dob = new Date(dob)
//   }
// }