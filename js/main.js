// DOM
const myForm = document.querySelector('#my-form')
const msg = document.querySelector('.msg')
const nameInput = document.querySelector("#name")
const emailInput = document.querySelector("#email")
const btn = document.querySelector('.btn')
const userlist = document.querySelector('#users')

myForm.addEventListener('submit', onSubmit)
function onSubmit(e) {
  e.preventDefault()
  if(nameInput.value === "" || emailInput.value === "") {
    msg.classList.add('error')
    msg.innerHTML = 'Isii gblkk'
    setTimeout( () => {
      msg.remove()
    }, 1000 )
  } else {
    const li = document.createElement('li');
    li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`));
    userlist.appendChild(li)

    nameInput.value = ''
    emailInput.value = ''
  }
}